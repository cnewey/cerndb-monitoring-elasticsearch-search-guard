Summary	:	This package contains the tcollector tools to send metrics to OpenTSDB.
Name:		cerndb-monitoring-elasticsearch-search-guard
Version:	0.0.1
Release:	1%{?dist}
Requires:       elasticsearch
BuildRequires:  elasticsearch
License:	GPL
BuildArch:	noarch
Group:		Development/Tools
Source:		%{name}-%{version}.tar.gz
BuildRoot:	%{_builddir}/%{name}-root
AutoReqProv:	no


%description
This package contains the tcollector tools to send metrics to OpenTSDB.

%prep
echo "This is prep"
%setup

%build
echo "This is build"

%install
echo "This is install"

cp -a ./search-guard-16-0.6-SNAPSHOT.zip /tmp/search-guard-16-0.6-SNAPSHOT.zip
/usr/share/elasticsearch/bin/plugin -i search-guard -u file:///tmp/search-guard-16-0.6-SNAPSHOT.zip

%clean
echo "This is clean"

%files
%defattr(-,root,root,-)
/usr/share/elasticsearch/plugins/search-guard/*

%changelog
* Wed Jul 22 2015 Charles Newey <charles.newey@cern.ch> 0.0.1-1
- Initial creation of search-guard package.
